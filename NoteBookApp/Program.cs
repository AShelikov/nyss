﻿using System;
using System.Collections.Generic;

namespace NoteBookApp
{
    class Notebook
    {
        public static List<Note> list = new List<Note>();
        static void Main(string[] args)
        {
            Console.WriteLine("[Записная книжка]");
            Console.WriteLine("Список команд:");
            Console.WriteLine("list - список контактов");
            Console.WriteLine("open [id] - открыть контакт");
            Console.WriteLine("create - добавить контакт");
            Console.WriteLine("delete [id] - удалить контакт");
            Console.WriteLine("edit [id] - редактировать Контакт");

            list.Add(new Note("Шеликов", "Артем", "Андреевич", 25211, "Россия", DateTime.Parse("25.03.2000"), "Московский Политех", "Студент", "Студент 3го курса ФИТ"));

            while (true)
            {
                Checker(Console.ReadLine());
            }
        }
        //Обработка команд
        public static void Checker(string comm)
        {
            string[] temp;
            try
            {
                if (comm == "list")
                {
                    for (int i = 0; i < list.Count; i++)
                    {
                        list[i].ReadShortNote(i);
                        Console.WriteLine();
                    }
                    if (list.Count == 0) Console.WriteLine("Контакты в книге отсутствуют");
                }
                else if (comm == "create")
                {
                    Note.CreateNewNote();
                }
                else if (comm.Split(' ').Length == 2)
                {
                    temp = comm.Split(' ');
                    if (temp[0] == "open") list[Int32.Parse(temp[1])].ReadNote(Int32.Parse(temp[1]));
                    else if (temp[0] == "edit") list[Int32.Parse(temp[1])].EditNote(Int32.Parse(temp[1]));
                    else if (temp[0] == "delete")
                    {
                        list.RemoveAt(Int32.Parse(temp[1]));
                        Console.WriteLine("Удалён контакт " + temp[1]);
                    }
                }
                else Console.WriteLine("Неверная команда, попробуйте ещё раз!");
            }
            catch (ArgumentOutOfRangeException)
            {
                Console.WriteLine("Контакт отсутствует!");
            }
        }
    }

    class Note
    {
        public string Surname;
        public string Name;
        public string Fathername;
        public int Number;
        public string Country;
        public DateTime Birthdate;
        public string Organization;
        public string Doljnost;
        public string Other;

        public Note(string surname, string name, string fathername, int number, string country, DateTime birthdate, string organization, string doljnost, string other)
        {
            Surname = surname;
            Name = name;
            Fathername = fathername;
            Number = number;
            Country = country;
            Birthdate = birthdate;
            Organization = organization;
            Doljnost = doljnost;
            Other = other;
        }
        //Создать контакт
        public static void CreateNewNote()
        {
            string surname;
            string name;
            string fathername;
            int number;
            string country;
            DateTime birthdate;
            string organization;
            string doljnost;
            string other;
            try
            {
                Console.Write("Фамилия: "); surname = Console.ReadLine();
                while (surname == "") { Console.WriteLine("Введите фамилию: "); surname = Console.ReadLine(); }
                Console.Write("Имя: "); name = Console.ReadLine();
                while (name == "") { Console.WriteLine("Введите имя: "); name = Console.ReadLine(); }
                Console.Write("Отчество: "); fathername = Console.ReadLine();
                Console.Write("Номер телефона: "); number = Int32.Parse(Console.ReadLine());
                while (number == 0) { Console.WriteLine("Введите номер: "); number = Int32.Parse(Console.ReadLine()); }
                Console.Write("Страна: "); country = Console.ReadLine();
                while (country == "") { Console.WriteLine("Введите страну: "); country = Console.ReadLine(); }
                Console.Write("Дата рождения: "); birthdate = DateTime.Parse(Console.ReadLine());
                Console.Write("Организация: "); organization = Console.ReadLine();
                Console.Write("Должность: "); doljnost = Console.ReadLine();
                Console.Write("Прочие заметки: "); other = Console.ReadLine();
                Notebook.list.Add(new Note(surname, name, fathername, number, country, birthdate, organization, doljnost, other));
            }
            catch (FormatException)
            {
                Console.WriteLine("Что-то пошло не так, неверный формат");
            }

        }
        //Изменить контакт
        public void EditNote(int id)
        {
            string surname;
            string name;
            string fathername;
            int number;
            string country;
            DateTime birthdate;
            string organization;
            string doljnost;
            string other;
            try
            {
                Console.Write("Фамилия: "); surname = Console.ReadLine();
                while (surname == "") { Console.WriteLine("Введите фамилию: "); surname = Console.ReadLine(); }
                Console.Write("Имя: "); name = Console.ReadLine();
                while (name == "") { Console.WriteLine("Введите имя: "); name = Console.ReadLine(); }
                Console.Write("Отчество: "); fathername = Console.ReadLine();
                Console.Write("Номер телефона: "); number = Int32.Parse(Console.ReadLine());
                while (number == 0) { Console.WriteLine("Введите номер: "); number = Int32.Parse(Console.ReadLine()); }
                Console.Write("Страна: "); country = Console.ReadLine();
                while (country == "") { Console.WriteLine("Введите страну: "); country = Console.ReadLine(); }
                Console.Write("Дата рождения: "); birthdate = DateTime.Parse(Console.ReadLine());
                Console.Write("Организация: "); organization = Console.ReadLine();
                Console.Write("Должность: "); doljnost = Console.ReadLine();
                Console.Write("Прочие заметки: "); other = Console.ReadLine();
                Note pahom = new Note(surname, name, fathername, number, country, birthdate, organization, doljnost, other);
                Notebook.list[id] = pahom;
            }
            catch (FormatException)
            {
                Console.WriteLine("Что-то пошло не так, неверный формат");
            }
        }

        
        //Вывод информации о контакте
        public void ReadNote(int id)
        {
            Console.WriteLine("ID: " + id);
            Console.WriteLine("Фамилия: " + Surname);
            Console.WriteLine("Имя: " + Name);
            Console.WriteLine("Отчество: " + Fathername);
            Console.WriteLine("Телефон: " + Number);
            Console.WriteLine("Страна: " + Country);
            Console.WriteLine("Дата рождения: " + Birthdate);
            Console.WriteLine("Организация: " + Organization);
            Console.WriteLine("Должность: " + Doljnost);
            Console.WriteLine("Прочие заметки: " + Other);
           
        }
        //Вывод краткой информации о контакте
        public void ReadShortNote(int id)
        {
            Console.WriteLine("ID: " + id);
            Console.WriteLine("Фамилия: " + Surname);
            Console.WriteLine("Имя: " + Name);
            Console.WriteLine("Телефон: " + Number);
        }
        
    }

}